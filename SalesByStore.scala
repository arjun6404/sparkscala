package org.spark.scala

 import org.apache.spark.SparkConf
  import org.apache.spark.SparkContext

object SalesByStore {
  def main(args : Array[String]) {
    
    val conf = new SparkConf()
    conf.set("spark.master","local")
    conf.set("spark.App.Name","SalesByStore")
				  
				val sc = new SparkContext(conf)
       val res = sc.range(1,100)
				res.collect.foreach(println)
				sc.stop()
    
  }
  
}